<?php

namespace PrincipalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $palabra = "Saludos";
        $colores = array("azul", "morado", "rojo", "naranja", "amarillo", "verde");
        return $this->render('PrincipalBundle:Default:index.html.twig', array('colores' => $colores, 'palabra' => $palabra));
    }
}
