<?php

namespace PrincipalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comentario
 *
 * @ORM\Table(name="comentario", indexes={@ORM\Index(name="usuariopkcomenfk", columns={"id_usuario"}), @ORM\Index(name="postpkcomenfk", columns={"id_post"})})
 * @ORM\Entity
 */
class Comentario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_comentario", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idComentario;

    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="string", length=150, nullable=false)
     */
    private $comentario;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_comentario", type="datetime", nullable=false)
     */
    private $fechaComentario;

    /**
     * @var \PrincipalBundle\Entity\Post
     *
     * @ORM\ManyToOne(targetEntity="PrincipalBundle\Entity\Post")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_post", referencedColumnName="id_post")
     * })
     */
    private $idPost;

    /**
     * @var \PrincipalBundle\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="PrincipalBundle\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     * })
     */
    private $idUsuario;



    /**
     * Get idComentario
     *
     * @return integer
     */
    public function getIdComentario()
    {
        return $this->idComentario;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     *
     * @return Comentario
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set fechaComentario
     *
     * @param \DateTime $fechaComentario
     *
     * @return Comentario
     */
    public function setFechaComentario($fechaComentario)
    {
        $this->fechaComentario = $fechaComentario;

        return $this;
    }

    /**
     * Get fechaComentario
     *
     * @return \DateTime
     */
    public function getFechaComentario()
    {
        return $this->fechaComentario;
    }

    /**
     * Set idPost
     *
     * @param \PrincipalBundle\Entity\Post $idPost
     *
     * @return Comentario
     */
    public function setIdPost(\PrincipalBundle\Entity\Post $idPost = null)
    {
        $this->idPost = $idPost;

        return $this;
    }

    /**
     * Get idPost
     *
     * @return \PrincipalBundle\Entity\Post
     */
    public function getIdPost()
    {
        return $this->idPost;
    }

    /**
     * Set idUsuario
     *
     * @param \PrincipalBundle\Entity\Usuario $idUsuario
     *
     * @return Comentario
     */
    public function setIdUsuario(\PrincipalBundle\Entity\Usuario $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \PrincipalBundle\Entity\Usuario
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}
