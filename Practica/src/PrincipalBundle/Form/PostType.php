<?php

namespace PrincipalBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nombre')
        ->add('descripcion', TextareaType::class)
        ->add('imagen', FileType::class)
        ->add('fechaPost', DateTimeType::class, array('date_widget'=>'single_text', 'format'=>'yyyy-MM-dd HH:mm:ss'))
        ->add('idUsuario', EntityType::class, array('class'=> 'PrincipalBundle:usuario', 'empty_value'=>'Seleccione','empty_data'=>null, 'property'=>'usuario'));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PrincipalBundle\Entity\Post'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'principalbundle_post';
    }


}
